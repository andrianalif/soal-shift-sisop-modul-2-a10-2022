#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>

void createFolder(char *locdir, char *category){
	char folderName[100];
	strcpy(folderName, locdir);
	strcat(folderName, "/");
	strcat(folderName, category);

	printf("%s\n", folderName);

	pid_t child_id;
	int status;
	child_id = fork();

	if (child_id < 0)
	{
		exit(EXIT_FAILURE);
	}

	if (child_id == 0)
	{
		char *argv[] = {"mkdir", "-p", folderName, NULL};
		execv("/bin/mkdir", argv);
	}
	else
	{
		while ((wait(&status)) > 0)
			;
		return;
	}
}

void unzip(char *zipdir, char *locdir){
	pid_t child_id;
	child_id = fork();
	int status;

	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	if (child_id == 0){
		char *argv[] = {"unzip", "-j", zipdir, "animal/*", "-d", locdir, NULL};
		execv("/bin/unzip", argv);
	}
	else{
		while ((wait(&status)) > 0);
		return;
	}
}

void movePhoto(char *fileName, char *category, char *locdir){
	char srcFileDir[100];
	strcpy(srcFileDir, locdir);
	strcat(srcFileDir, "/");
	strcat(srcFileDir, fileName);

	char destFileDir[100];
	strcpy(destFileDir, locdir);
	strcat(destFileDir, "/");
	strcat(destFileDir, category);
	strcat(destFileDir, "/");
	strcat(destFileDir, fileName);

	pid_t child_id;
	child_id = fork();
	int status;

	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	if (child_id == 0){
		char *argv[] = {"mv", srcFileDir, destFileDir, NULL};
		execv("/bin/mv", argv);
	}
	else{
		while ((wait(&status)) > 0);
		return;
	}
}

void deletePhoto(char *fileName, char *locdir){
	char fileDir[100];
	strcpy(fileDir, locdir);
	strcat(fileDir, "/");
	strcat(fileDir, fileName);

	pid_t child_id;
	child_id = fork();
	int status;

	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	if (child_id == 0){
		char *argv[] = {"rm", fileDir, NULL};
		execv("/bin/rm", argv);
	}
	else{
		while ((wait(&status)) > 0)
			;
		return;
	}
}

int main(){
	char *locdir = "/home/andrianalif/praktikum2/soal3";
	char *zipdir = "/home/andrianalif/animal.zip";

	createFolder(locdir, "darat");
	sleep(3);
	createFolder(locdir, "air");
	unzip(zipdir, locdir);

	DIR *dp;
	struct dirent *ep;
	dp = opendir(locdir);
	if (dp != NULL){
		while ((ep = readdir(dp)) != NULL){
			if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0 && strcmp(ep->d_name, "darat") != 0 && strcmp(ep->d_name, "air") != 0){
				if (strstr(ep->d_name, "darat") != NULL){
					movePhoto(ep->d_name, "darat", locdir);
				}
				else if (strstr(ep->d_name, "air") != NULL){
					movePhoto(ep->d_name, "air", locdir);
				}
				else{
					deletePhoto(ep->d_name, locdir);
				}
			}
		}
	}
	closedir(dp);

	char daratDir[100];
	strcpy(daratDir, locdir);
	strcat(daratDir, "/darat");
	dp = opendir(daratDir);
	if (dp != NULL){
		while ((ep = readdir(dp)) != NULL){
			if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
				if (strstr(ep->d_name, "bird") != NULL){
					deletePhoto(ep->d_name, daratDir);
				}
			}
		}
	}
	closedir(dp);

	char airDir[100];
	strcpy(airDir, locdir);
	strcat(airDir, "/air");
	dp = opendir(airDir);
	if (dp != NULL){
		while ((ep = readdir(dp)) != NULL){
			struct stat info;
			int r;
			if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
				char listFile[100];
				strcpy(listFile, airDir);
				strcat(listFile, "/list.txt");

				char pathFile[100];
				strcpy(pathFile, airDir);
				strcat(pathFile, "/");
				strcat(pathFile, ep->d_name);
				
				FILE *fp = fopen(listFile, "a");
				r = stat(pathFile, &info);

				fprintf(fp, "%s_", getpwuid(info.st_uid)->pw_name);

				if (info.st_mode & S_IRUSR && info.st_mode & S_IWUSR && info.st_mode & S_IXUSR)
					fprintf(fp, "rwx");
				else if (info.st_mode & S_IRUSR && info.st_mode & S_IWUSR)
					fprintf(fp, "rw");
				else if (info.st_mode & S_IRUSR && info.st_mode & S_IXUSR)
					fprintf(fp, "rx");
				else if (info.st_mode & S_IRUSR)
					fprintf(fp, "r");
				else if (info.st_mode & S_IWUSR && info.st_mode & S_IXUSR)
					fprintf(fp, "wx");
				else if (info.st_mode & S_IWUSR)
					fprintf(fp, "w");
				else if (info.st_mode & S_IXUSR)
					fprintf(fp, "x");

				char *name = strtok(ep->d_name, "_");
				fprintf(fp, "_%s\n", name);
				fclose(fp);
			}
		}
	}
	closedir(dp);
}

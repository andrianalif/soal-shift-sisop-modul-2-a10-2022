#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#include <wait.h>

void sistem(char command[], char *argv[]) {
    pid_t child_id;
    int status;
    
    child_id = fork();

    if(child_id == 0) {
        execv(command, argv);
    } else {
 while ((wait(&status)) > 0);
    }
}
//fungsi membuat direktori dan ekstrak file zip 
void unzip() {
    pid_t child_id;
    int status;
    
    child_id = fork();

    if(child_id < 0) {
       exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        char *argv[] = {"mkdir","-p","drakor",NULL};
        sistem("/bin/mkdir", argv);
        
    } else {
        while ((wait(&status))>0);
        char *argv2[] = {"unzip","drakor.zip","-x" , "*/*", "-d", "drakor", NULL}; 
        sistem("/bin/unzip", argv2);
    }
}

char* delete(char* c){
    int n, i;
    char* cut;
    for (i = 0; c[i] != '\0'; i++);
    n = i - 3;
    if (n < 1) return NULL;

    cut = (char*) malloc (n * sizeof(char));

    for (i = 0; i < n - 1; i++) cut[i] = c[i];

    cut[i] = '\0';
    return cut;
}

void directory() {
    pid_t child_id;
    int status;

    child_id = fork();

    if(child_id < 0) {
       exit(EXIT_FAILURE);
    }

    if (child_id==0) {
     DIR *dp = opendir("/home/gumilang/testingsoal2/drakor");
        if (dp != NULL) {
            struct dirent *ep; 
            while((ep = readdir(dp))!= NULL)
            {
                if(ep->d_type == DT_REG) {

                    char *temp1, *temp2, *temp3, *temp4;
                    char *FileName = ep->d_name;
                    char *NewName = delete(FileName);
                    char copy1[101], copy2[101], copy3[101], base2[101], base3[101];

//setiap delimiter (_)
                    for(temp1 = strtok_r(NewName,"_",&temp3); temp1 != NULL; temp1 = strtok_r(NULL,"_",&temp3)) {
                        char nama[101], rilis[101], jenis[101];
                        int data = 0;
                        char base[101]="/home/gumilang/testingsoal2/drakor/";

                        strcpy(copy1, ep->d_name);
                        strcpy(base2, base);
                        strcpy(base3, base);
                        strcpy(copy2, ep->d_name);
                        strcpy(copy3, ep->d_name);
//setiap delimiter (;)                        
                        for(temp2 = strtok_r(temp1,";",&temp4); temp2!=NULL; temp2=strtok_r(NULL,";",&temp4)) {
                                if(data == 0) strcpy(nama, temp2);
                                if(data == 1) strcpy(rilis, temp2);
                                if(data == 2) strcpy(jenis, temp2);
                                data += 1;
                        }
                        
                        strcat(base,jenis);
                        char *argmk[] = {"mkdir","-p",base,NULL};
                        sistem("/bin/mkdir",argmk);


                        char txtLoc[100], txtPath[100];
                        strcpy(txtLoc,base);
                        stpcpy(txtPath,txtLoc);

                        char NameText[100], cnt[100];
                        strcpy(NameText,nama);

                        strcat(nama,".png");
                        strcat(base2,copy2);

                        char *move[] = {"cp","-r", base2, base, NULL};
                        sistem("/bin/cp",move);

                        
                        strcpy(base3,base);
                        strcat(base3,"/");
                        strcat(base3,copy2);
                        strcat(base,"/");
                        strcat(base,nama);

                        char *rename[] = {"mv",base3,base,NULL};
                        sistem("/bin/mv",rename);
                        strcat(txtPath,"/data.txt");

                        strcpy(cnt,"nama : ");
                        strcat(cnt,NameText);
                        strcat(cnt,"\n");
                        strcat(cnt,"rilis: tahun ");
                        strcat(cnt,rilis);
                        strcat(cnt,"\n");

                        FILE *fp;
                        fp=fopen(txtPath,"a");
                        fputs(cnt,fp);
                        fclose(fp);
                    }
                }

                if(ep->d_type == DT_REG) {
                    char base[101]="/home/gumilang/testingsoal2/drakor/";
                    strcat(base,ep->d_name);

                    char *hapus[] = {"rm","-rf",base,NULL};
                    sistem("/bin/rm",hapus);
                }
            }
        }
        closedir (dp);
    } else {
        while ((wait(&status))>0);
   }
}

int main() {

		chdir ("/home/gumilang/testingsoal2");

    pid_t child_id;
    int status;

    child_id=fork();

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    }
   
    if(child_id == 0) {
        unzip();
 		directory();
    } else {
       while ((wait(&status))>0);
    }
}
